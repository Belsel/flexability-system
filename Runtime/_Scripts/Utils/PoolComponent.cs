using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

public class PoolComponent : MonoBehaviour
{
	public Pool Pool;
	
	/// <summary>
    /// Creates a new instance of the pool object.
    /// </summary>
    /// <param name="prefab">The pool object prefab to create an instance of.</param>
    /// <returns>The newly created instance of the pool object.</returns>
    private PoolObject CreateInstance(PoolObject prefab, Transform parent)
    {
        GameObject instance = Instantiate(prefab.gameObject, parent);
        instance.gameObject.SetActive(false);
        PoolObject poolObject = instance.GetComponent<PoolObject>();
        poolObject.Disable += ReturnObjectToPool;
        return poolObject;
    }

    /// <summary>
    /// Activates the object when taken from the object pool.
    /// </summary>
    /// <param name="instance">The object instance to activate.</param>
    private void OnTakeFromPool(PoolObject instance)
    {
        instance.gameObject.SetActive(true);
    }

    /// <summary>
    /// Deactivates the object when returned to the object pool.
    /// </summary>
    /// <param name="instance">The object instance to deactivate.</param>
    private void OnReturnToPool(PoolObject instance)
    {
        instance.gameObject.SetActive(false);
    }

    /// <summary>
    /// Returns the object instance to its corresponding object pool.
    /// </summary>
    /// <param name="instance">The object instance to return to the pool.</param>
    private void ReturnObjectToPool(PoolObject instance)
    {
        ObjectPool<PoolObject> pool = Pool.PoolList;
        pool.Release(instance);
    }

    /// <summary>
    /// Creates a new object pool for the given object instance.
    /// </summary>
    /// <param name="instance">The object instance to create a pool for.</param>
    /// <param name="startingSize">The initial size of the object pool.</param>
    /// <param name="maxSize">The maximum size of the object pool.</param>
    public void CreatePool(Pool pool)
    {
        Pool = pool;
        Pool.ID = Pool.Prefab.ID;
        gameObject.name = $"{Pool.ID}";
        ObjectPool<PoolObject> poolList = new(
            () => CreateInstance(Pool.Prefab, transform),
            OnTakeFromPool,
            OnReturnToPool,
            OnDestroyObject,
            false,
            Pool.Prefab._startingAmount,
            Pool.Prefab._maxAmount
        );
        Pool.PoolList = poolList;
    }

    /// <summary>
    /// Destroys the game object of the given instance.
    /// </summary>
    /// <param name="instance">The instance of the pool object to destroy.</param>
    private void OnDestroyObject(PoolObject instance)
    {
        Destroy(instance.gameObject);
    }
}
