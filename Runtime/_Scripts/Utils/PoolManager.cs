using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

/// <summary>
/// Manages a set of object pools for recycling objects in Unity.
/// </summary>
public class PoolManager : MonoBehaviour
{
    public static PoolManager Instance;

    /// <summary>
    /// Initializes the object pools when the script instance is being loaded.
    /// </summary>
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }



    /// <summary>
    /// Returns the object pool associated with the given pool object instance.
    /// </summary>
    /// <param name="instance">The pool object instance for which to retrieve the object pool.</param>
    /// <returns>The object pool associated with the given pool object instance. If no object pool is found, a warning is logged and null is returned.</returns>
    public ObjectPool<PoolObject> GetPool(PoolObject instance)
    {
        Transform tmpTransform = transform.Find(instance.ID);
        GameObject tmpGameObject = null;
        PoolComponent poolComponent;
        Pool pool = new()
        {
            Prefab = instance,
            ID = instance.ID,
            StartingAmount = instance._startingAmount,
            MaxAmount = instance._maxAmount,

        };
        if (tmpTransform != null)
        {
            tmpGameObject = tmpTransform.gameObject;
        }
        if (tmpGameObject == null)
        {
            tmpGameObject = new GameObject(instance.ID);
            tmpGameObject.transform.parent = transform;
        }
        poolComponent = tmpGameObject.GetComponent<PoolComponent>();
        if (poolComponent == null)
        {
            poolComponent = tmpGameObject.AddComponent<PoolComponent>();
            poolComponent.CreatePool(pool);
        }
        return poolComponent.Pool.PoolList;
    }
}
