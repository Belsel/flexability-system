using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Pool;

[System.Serializable]
public struct Pool
{
	private string _iD;
	[HideInInspector]
	public string ID { get => _iD; set => _iD = _iD == null ? value : _iD; }
	public PoolObject Prefab;
	public int StartingAmount;
	public int MaxAmount;
	public ObjectPool<PoolObject> PoolList;
}
