using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// A script to enable objects to be pooled.
/// </summary>
public class PoolObject : MonoBehaviour
{
    [SerializeField]
    private string _iD;
    public string ID { get => _iD; set => _iD = _iD == null ? value : _iD; }

    public int _startingAmount;
    public int _maxAmount;
    /// <summary>
    /// A delegate to handle the disabling of a pooled object.
    /// </summary>
    /// <param name="instance">The instance of the pooled object being disabled.</param>
    public delegate void OnDisableCallback(PoolObject instance);
    /// <summary>
    /// An event to be triggered when the object is disabled, which will call any registered callbacks.
    /// </summary>
    public event OnDisableCallback Disable;

    /// <summary>
    /// Raises the disable event, calling any registered callbacks.
    /// </summary>
    private void OnDisable()
    {
        Disable?.Invoke(this);
    }
}