using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InputState
{
    started,
    performed,
    canceled
}
