using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public abstract class Controller : MonoBehaviour
{
	[Header("Inputs")]
	protected Vector2 _movementDirection;

	[Header("Character")]
	protected CharacterComponent _possessedCharacter;
	public CharacterComponent PossessedCharacter { get => _possessedCharacter; }
	[SerializeField]
	private CharacterComponent _initialCharacter;

	private List<Coroutine> _coroutines;

	protected virtual void Start()
	{
		PossesCharacter(_initialCharacter);
	}

	protected virtual void Awake()
	{
	}

	/// <summary>
	/// Changes the controlled character by this controller
	/// </summary>
	/// <param name="character">The target character</param>
	protected void PossesCharacter(CharacterComponent character)
	{
		if (character == null)
		{
			return;
		}
		_possessedCharacter = character;
	}

	public void AddCoroutine(IEnumerator coroutine)
	{
		_coroutines.Add(StartCoroutine(coroutine));
	}

}
