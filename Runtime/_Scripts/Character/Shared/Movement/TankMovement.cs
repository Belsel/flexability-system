using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Movement/Tank")]
public class TankMovement : Movement
{
	public float RotationSpeed;
	public float MaxSpeed;
	public float Acceleration;

	/// <summary>
	/// Moves the character forward to the desired direction.
	/// </summary>
	/// <param name="direction">The desired direction</param>
	public override void Move(Vector2 direction, Rigidbody rigidbody)
	{
		Vector3 _desiredVelocity = new Vector3(direction.x, 0, direction.y) * MaxSpeed;

		rigidbody.velocity = Vector3.MoveTowards(rigidbody.velocity, _desiredVelocity, Time.deltaTime * Acceleration);
		if (direction.magnitude != 0)
		{
			rigidbody.gameObject.transform.rotation = Quaternion.Slerp(rigidbody.transform.rotation, Quaternion.LookRotation(_desiredVelocity), Time.deltaTime * RotationSpeed);
		}
	}
}

