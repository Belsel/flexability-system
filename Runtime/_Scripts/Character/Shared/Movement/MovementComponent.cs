using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MovementComponent : MonoBehaviour
{

    private Movement _data;
    private Rigidbody _rigidbody;
    private Vector2 _desiredDirection;
  

	private void Update()
	{
        Move(_desiredDirection);
	}

    public void LoadMovement(Movement data)
    {
        _data = data;
        _rigidbody = GetComponent<Rigidbody>();      
    }
    

	public void SetDesiredDirection(Vector2 desiredDirection)
	{
        _desiredDirection = desiredDirection;
	}
    public void Move(Vector2 direction)
	{
        if(_data == null || _rigidbody == null)
		{
            return;
		}
        _data.Move(direction, _rigidbody);
	}

}
