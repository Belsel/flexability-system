using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for launch damage scriptable objects
/// </summary>
[CreateAssetMenu(menuName = "Data/Effects/Damage")]
public class Damage : Effect
{
	[Header("Damage")]
	[Tooltip("The minimum damage the effect can deal")]
	public float MinDamage;
	[Tooltip("The maximum damage the effect can deal")]
	public float MaxDamage;

	/// <summary>
	/// Launches the damage effect
	/// </summary>
	/// <param name="data"></param>
	public override void Fire(EffectData data)
	{
		CharacterComponent target = data.Target.GetComponent<CharacterComponent>();
		if (target == null)
		{
			return;
		}
		target.Damage(Random.Range(MinDamage, MaxDamage));
	}
}
