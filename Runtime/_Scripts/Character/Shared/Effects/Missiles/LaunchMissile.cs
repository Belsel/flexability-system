using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Base class for launch missile scriptable objects
/// </summary>
[CreateAssetMenu(menuName = "Data/Effects/LaunchMissile")]
public class LaunchMissile : Effect
{
    [Header("Prefab")]
    [Tooltip("The prefab of the missile")]
    public GameObject MissilePrefab;
    [Header("Missile data")]
    [Tooltip("The target mask of the missile")]
    public LayerMask TargetMask;
    [Tooltip("If the missile has gravity")]
    public bool HasGravity = false;
    [Tooltip("The starting speed of the missile")]
    public Vector3 MissileStartingSpeed;
    [Tooltip("Missile collission radius")]
    public float CollisionRadius;
    [Tooltip("Missile collission height")]
    public float CollisionHeight;
    [Tooltip("The list of effects that will be fired when the missile impacts")]
    public Effect[] OnImpactEffects;
    [Header("Missile pool")]
    [Tooltip("The starting count of instances of missiles")]
    public int StartPoolCount = 10;
    [Tooltip("The max count of instances of missiles for this effect for each player")]
    public int MaxPoolCount = 100;

    public UnityEvent<EffectData> OnImpact;
    

    /// <summary>
	/// Gets the data payload from the impact and fires all the onImpact effects with the given context
	/// </summary>
	/// <param name="payload">The impact data</param>
	private void Impact(MissileImpactPayload payload)
    {
        EffectData data = new()
        {
            Target = payload.target
        };
        OnImpact?.Invoke(data);
    }

    /// <summary>
	/// Launches the missile projectile
	/// </summary>
	/// <param name="data"></param>
	public override void Fire(EffectData data)
    {
        if (data.EffectTransform == null && data.Parent != null)
        {
            data.EffectTransform = data.Parent.transform;
        }
        PoolObject poolObject = MissilePrefab.GetComponent<PoolObject>();
        GameObject instance;
        if (poolObject == null)
		{
            instance = Instantiate(MissilePrefab, data.Parent.transform);
		}
        else
		{
            instance = PoolManager.Instance.GetPool(poolObject).Get().gameObject;
		}
        instance.transform.SetPositionAndRotation(data.EffectTransform.position, data.EffectTransform.rotation);
        Rigidbody rigidbody = instance.GetComponent<Rigidbody>();
        Missile missile = instance.AddComponent<Missile>();
        missile.SetData(data);
        missile.OnImpactAction(Impact);
        CapsuleCollider collider = instance.GetComponent<CapsuleCollider>();
        if( collider == null)
		{
            collider = instance.AddComponent<CapsuleCollider>();
		}
        collider.radius = CollisionRadius;
        collider.height = CollisionHeight;
        collider.isTrigger = true;
        if (rigidbody == null)
        {
            rigidbody = instance.AddComponent<Rigidbody>();
        }
        rigidbody.useGravity = HasGravity;
        Vector3 startingSpeed = (MissileStartingSpeed.x * instance.transform.right) + (MissileStartingSpeed.y * instance.transform.up) + (MissileStartingSpeed.z * instance.transform.forward);
        rigidbody.velocity = startingSpeed;
    }
}
