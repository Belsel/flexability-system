using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Represents the data payload of a missile impact
/// </summary>
public struct MissileImpactPayload
{
    /// <summary>
    /// The character that was hit by the missile
    /// </summary>
    public CharacterComponent target;
}