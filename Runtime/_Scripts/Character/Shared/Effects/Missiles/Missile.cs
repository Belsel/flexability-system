using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Missile : MonoBehaviour
{

    [Header("Data")]
    private Action<MissileImpactPayload> _impactAction;
    protected CharacterComponent _parent;

    /// <summary>
    /// Sets the action to be called when the missile impacts with something
    /// </summary>
    /// <param name="action">The action to be called</param>
    public void OnImpactAction(Action<MissileImpactPayload> action)
    {
        _impactAction = action;
    }

	/// <summary>
	/// Called when the missile collides with something
	/// </summary>
	/// <param name="other">The collider the missile collided with</param>
	private void OnTriggerEnter(Collider other)
    {
        CharacterComponent target = other.gameObject.GetComponent<CharacterComponent>();
        if(other.gameObject == _parent.gameObject)
		{
            return;
		}
        if (target == null)
        {
            gameObject.SetActive(false);
            return;
        }
        MissileImpactPayload payload = new()
        {
            target = target
        };
        _impactAction(payload);
        gameObject.SetActive(false);
    }

    /// <summary>
    /// Sets the data for the missile's effect
    /// </summary>
    /// <param name="data">The data to be set</param>
    public void SetData(EffectData data)
    {
        if (data.EffectTransform != null)
        {
            transform.position = data.EffectTransform.position;
            transform.rotation = data.EffectTransform.rotation;
        }
        _parent = data.Parent;

    }

}