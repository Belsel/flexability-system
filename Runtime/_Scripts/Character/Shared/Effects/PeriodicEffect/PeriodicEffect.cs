using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

// This attribute adds the "PeriodicEffect" asset creation option to the "Create Asset" menu.
[CreateAssetMenu(menuName = "Data/Effects/PeriodicEffect")]
public class PeriodicEffect : Effect
{
	[Tooltip("The initial effects that will fire when this effect is activated.")]
	public UnityEvent<EffectData> OnInit;

    [Tooltip("The effects that will fire periodically.")]
	public UnityEvent<EffectData> OnPeriodic;

	[Tooltip("The effects that will fire when the periodic effects are finished.")]
	public UnityEvent<EffectData> OnEnd;

	[Tooltip("The time to wait between each repetition of the periodic effects.")]
    public float[] WaitTime;

    [Tooltip("The number of times to repeat the periodic effects.")]
    public int Repetitions;

	/// <summary>
	/// The coroutine that triggers the PeriodicEffect.
	/// </summary>
	/// <param name="data">The data associated with the PeriodicEffect.</param>
	/// <returns>An IEnumerator representing the coroutine.</returns>
	private IEnumerator PeriodicCoroutine(EffectData data)
	{
		OnInit?.Invoke(data);
		int counter = 0;
		while (counter < Repetitions)
		{
			OnPeriodic?.Invoke(data);
			yield return new WaitForSeconds(WaitTime[counter % WaitTime.Length]);
		}
		OnEnd?.Invoke(data);

	}
	/// <summary>
	/// Fires the PeriodicEffect.
	/// </summary>
	/// <param name="data">The data associated with the PeriodicEffect.</param>
	public override void Fire(EffectData data)
	{
		if (data.Player != null)
		{
			data.Player.AddCoroutine(PeriodicCoroutine(data));
		}
	}
}