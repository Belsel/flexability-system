using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for all effect base data
/// </summary>
public abstract class Effect : ScriptableObject
{
	public abstract void Fire(EffectData data);
	
}
