using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Data/Effects/SearchArea")]
public class SearchArea : Effect
{
	public UnityEvent<EffectData> OnImpact;
	public UnityEvent<EffectData> OnPerCollision;
    public float Radius;
    public int MaxTargets;

	public override void Fire(EffectData data)
	{
		OnImpact?.Invoke(data);
		List<CharacterComponent> targets = new();
		Collider[] colliders= Physics.OverlapSphere(data.TargetPoint, Radius);
		foreach (Collider collider in colliders)
		{
			CharacterComponent target = collider.GetComponent<CharacterComponent>();
			if (target != null)
			{
				targets.Add(target);
			}
		}
		float maxCount = Mathf.Min(targets.Count, MaxTargets);
		for (int i = 0; i < maxCount; ++i)
		{
			data.Target = targets[i];
			OnPerCollision?.Invoke(data);
		}
	}
}
