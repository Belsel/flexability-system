using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct EffectData
{
    public CharacterComponent Target;
    public CharacterComponent Parent;
    public Controller Player;
    public LayerMask TargetMask;
    public Transform EffectTransform;
    public Vector3 TargetPoint;
}
