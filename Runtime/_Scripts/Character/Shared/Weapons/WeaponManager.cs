using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Manages the weapons of a character
/// </summary>
public class WeaponManager : MonoBehaviour
{
    /// <summary>
    /// The list of weapons the character can use
    /// </summary>
    [SerializeField]
    private List<WeaponComponent> _weaponList;

    /// <summary>
    /// The index of the current weapon the character is using
    /// </summary>
    [SerializeField]
    private int _currentWeapon;    

    public int CurrentWeapon { get => _currentWeapon; set => _currentWeapon = value % _weaponList.Count; }

    public void UseWeapon(EffectData data, InputState state, int index)
	{
        if(_weaponList.Count < 1)
		{
            return;
		}
        if(_weaponList[_currentWeapon] == null)
		{
            return;
		}
        _weaponList[_currentWeapon].UseWeapon(data, state, index);
	}    
}
