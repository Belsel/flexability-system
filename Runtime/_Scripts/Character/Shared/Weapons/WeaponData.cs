using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public struct WeaponData 
{
	public UnityEvent<EffectData> OnShoot;
	public ShootingType ShootingType;
	public int AmmunitionCost;
	public float Cooldown;
}
