using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum ShootingType { single, automatic, charge }

[CreateAssetMenu(menuName = "Data/Weapon")]
public class Weapon : ScriptableObject
{
    [Header("Weapon")]
    public int MaxAmmo;

	public WeaponData[] WeaponData;
	

	public void FireWeapon(EffectData data, InputState state, int index)
	{
		Fire(data, WeaponData[index]);
	}



	public void Fire(EffectData data, WeaponData weaponData)
	{
		weaponData.OnShoot?.Invoke(data);
	}
}
