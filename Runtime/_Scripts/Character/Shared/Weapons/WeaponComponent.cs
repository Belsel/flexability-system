using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponComponent : MonoBehaviour
{
    [SerializeField]
    private Weapon _data; // Reference to the Weapon data that this component is attached to
    public Weapon Data { get => _data;  }

    [SerializeField]
    private Transform[] _shootTransform; // Reference to the primary shot transform for this weapon
    private int _currentTransform;

    [SerializeField]
    private float _cooldownTimer;
    private bool _isWeaponEnabled = true;
    public bool IsWeaponEnabled { get; set; }

    // Property for accessing the primary shot transform from outside the class
    public Transform GetShootTransform()
    {
        Transform transform;
        if (_shootTransform.Length > 0)
        {
            transform = _shootTransform[_currentTransform % _shootTransform.Length];
            ++_currentTransform;
        }
        else
        {
            transform = gameObject.transform;
        }
        return transform;
    }

	private void Update()
	{
        _cooldownTimer += Time.deltaTime;
	}

	/// <summary>
	/// Uses the weapon's primary fire mode with the given effect data and input state.
	/// </summary>
	/// <param name="data">The effect data to use for the primary fire.</param>
	/// <param name="state">The input state to use for the primary fire.</param>
	public void UseWeapon(EffectData data, InputState state, int index)
    {
        if(_cooldownTimer >= Data.WeaponData[index].Cooldown)
		{
            data.EffectTransform = GetShootTransform();

            // Call the primary fire method of the attached Weapon data with the given effect data and input state
            _data.FireWeapon(data, state, index);
            _cooldownTimer = 0;
		}
    }


}