using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CharacterComponent : MonoBehaviour
{
    [Header("Stats")]
    protected float _healthPoints; // Current health points of the character
    public float HealthPoints { get => _healthPoints; set => _healthPoints = Mathf.Clamp(value, 0, _data.MaxHP); }
    [SerializeField]
    private Character _data; // Scriptable object that stores character data
    public Character Data { get => _data; set => _data = value; } // Getter and setter for character data
    [SerializeField]
    private Controller _owner; // The controller that owns this character
    public Controller Owner { get => _owner; set => _owner = value; } // Getter and setter for the character's owner
    protected Action<CharacterComponent> _storeAction; // Action to take when the character dies
    protected MovementComponent _movement; // Component that handles movement for the character
    protected WeaponManager _weaponManager; // Component that handles weapons for the character

	private void Awake()
	{
        _movement = GetComponent<MovementComponent>(); // Get the Movement component on this character
        _weaponManager = GetComponent<WeaponManager>(); // Get the WeaponManager component on this character
        _movement.LoadMovement(_data.movement);
        HealthPoints = _data.MaxHP;
    }
	private void Start()
    {
    }

    /// <summary>
    /// Set the action to take when the character dies
    /// </summary>
    public void Store(Action<CharacterComponent> action)
    {
        _storeAction = action;
    }

    /// <summary>
    /// Reduce health points by the given damage amount and die if health points reach zero or below
    /// </summary>
    public void Damage(float damage)
    {
        HealthPoints = Mathf.Max(0, _healthPoints - damage); // Reduce health points by the given damage amount
        if (HealthPoints <= 0) // If the character's health points have reached zero or below
        {
            Die(); // Die
        }
    }

    /// <summary>
    /// Call the action to take when the character dies
    /// </summary>
    protected void Die()
    {
        if (_storeAction != null)
        {
            gameObject.SetActive(false);
        }
    }

    public void SetDesiredDirection(Vector2 direction)
	{
        _movement.SetDesiredDirection(direction);
	}

    /// <summary>
    /// Placeholder method for using a primary item
    /// </summary>
    public void UseItem(EffectData data, InputState state,int index)
    {
        data.Parent = this;
        _weaponManager.UseWeapon(data, state, index);
    }
}