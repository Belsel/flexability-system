using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/Character")]
public class Character : ScriptableObject
{
    public float MaxHP;
    public Movement movement;
}
